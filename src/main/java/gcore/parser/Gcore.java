package gcore.parser;

import org.apache.commons.vfs2.FileObject;
import org.metaborg.core.MetaborgException;
import org.metaborg.core.language.ILanguageComponent;
import org.metaborg.core.language.ILanguageDiscoveryRequest;
import org.metaborg.core.language.ILanguageImpl;
import org.metaborg.core.language.LanguageUtils;
import org.metaborg.core.syntax.ParseException;
import org.metaborg.spoofax.core.Spoofax;
import org.metaborg.spoofax.core.unit.ISpoofaxInputUnit;
import org.metaborg.spoofax.core.unit.ISpoofaxParseUnit;
import org.spoofax.interpreter.terms.IStrategoTerm;

import java.io.IOException;
import java.net.URL;
import java.util.Set;

public class Gcore implements AutoCloseable {
    private static final String GCORE_GRAMMAR_SPEC = "gcore-spoofax-0.1.0-SNAPSHOT.spoofax-language";
    private final ILanguageImpl gcore;
    private final Spoofax spoofax;

    public Gcore() throws MetaborgException {
        spoofax = new Spoofax();
        gcore = loadGcore(spoofax);
        if (gcore == null) {
            throw new MetaborgException("No language implementation was found");
        }
        System.out.println("Loaded " + gcore);
    }

    /** Loads the G-CORE language component. */
    private ILanguageImpl loadGcore(Spoofax spoofax) throws MetaborgException {
        URL gcoreUrl = Gcore.class.getClassLoader().getResource(GCORE_GRAMMAR_SPEC);
        FileObject gcoreLocation = spoofax.resourceService.resolve("zip:" + gcoreUrl + "!/");
        Iterable<ILanguageDiscoveryRequest> requests =
                spoofax.languageDiscoveryService.request(gcoreLocation);
        Iterable<ILanguageComponent> components =
                spoofax.languageDiscoveryService.discover(requests);
        Set<ILanguageImpl> implementations = LanguageUtils.toImpls(components);
        ILanguageImpl gcore = LanguageUtils.active(implementations);
        return gcore;
    }

    /** Parses a G-CORE query from a file. */
    public IStrategoTerm parseQuery(String fileName) throws IOException, ParseException {
        FileObject gcoreFile = spoofax.resourceService.resolve("res:" + fileName);
        String gcoreContents = spoofax.sourceTextService.text(gcoreFile);
        ISpoofaxInputUnit input = spoofax.unitService.inputUnit(gcoreFile, gcoreContents, gcore, null);
        ISpoofaxParseUnit output = spoofax.syntaxService.parse(input);
        if(!output.valid()) {
            System.out.println("Could not parse " + fileName);
            return null;
        }
        IStrategoTerm ast = output.ast();
        return ast;
    }

    /** Not the best pretty print. */
    public void prettyPrintAst(IStrategoTerm ast) {
        prettyPrintAst(ast, 1);
    }

    private void prettyPrintAst(IStrategoTerm ast, int indentLevel) {
        String termName = ast.toString().split("\\(")[0];
        System.out.println(String.format("%" + indentLevel + "s%s", "", termName));
        for (IStrategoTerm subTerm : ast.getAllSubterms()) {
            prettyPrintAst(subTerm, indentLevel + 2);
        }
    }

    @Override
    public void close() throws Exception {
        spoofax.close();
    }
}

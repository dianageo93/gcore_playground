import gcore.parser.Gcore
import org.spoofax.interpreter.terms.{IStrategoAppl, IStrategoTerm}

object GcoreRunner extends App {
  val PlaygroundQuery = "gcore-queries/playground.gcore"

  // Non-exhaustive pretty-printer.
  private def prettyPrint(term: IStrategoTerm, indent: Int): Unit =
    term.getTermType match {
      case IStrategoTerm.APPL =>
        var appl: IStrategoAppl = term.asInstanceOf[IStrategoAppl]
        println(" " * indent + appl.getConstructor.getName)
        term.getAllSubterms.foreach(subTerm => prettyPrint(subTerm, indent + 2))

      case IStrategoTerm.STRING => println(" " * indent + term.toString())

      case IStrategoTerm.LIST =>
        term.getAllSubterms.foreach(subTerm => prettyPrint(subTerm, indent + 2))

      case _ => println(" " * indent + "cannot print this term " + term.getTermType)
    }

  val gcore: Gcore = new Gcore()
  val ast: IStrategoTerm = gcore.parseQuery(PlaygroundQuery)

  prettyPrint(ast, 0)
}
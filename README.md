Files in this repo:

- src/main/java/gcore/parser -> Code to work with a Spoofax object built from the GCORE language component.

- src/main/scala -> Main code for the GCORE parser, uses the Spoofax output.


To run from command line:

```
$ mvn package
$ java -jar target/parser_test-1.0-SNAPSHOT-jar-with-dependencies.jar 
```